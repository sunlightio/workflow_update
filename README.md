# Workflow Update Scripts

This repository contains update scripts for the Evolve project workflow scripts.

(C)2019 Sunlight.io. All Rights Reserved.

## Requirements

**Important** The update scripts require access to the private code of the workflow interpreter and specification.
This is only accessible by project partners.

## Two ways to update:

### 1. Clone the repository and execute the script

git clone https://bitbucket.org/sunlightio/workflow_update.git

workflow_update/zeppelin/evolve_update.sh

### 2. Execute remote script directly:

Fror the Zeppelin VM vagrant user home dir, execute:

curl -fsSL https://bitbucket.org/sunlightio/workflow_update/raw/471bf91e858e2bf2bb020238d8735d92bab84a83/zeppelin/evolve_update.sh | bash

**NOTE:** for the actual raw script URL on bitbucket.org, please check the raw link at:

https://bitbucket.org/sunlightio/workflow_update/
