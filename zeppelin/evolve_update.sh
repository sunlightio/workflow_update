#!/bin/bash
#set -x
VAGRHOME=/home/vagrant
WORKPATH="${WORKPATH:-${VAGRHOME}/workflows}"
RMWORKPATH="${RMWORKPATH:-false}"
REPODIR="${REPODIR:-workflow_process_cicd}"
REPOHOST="${REPOHOST:-evolve.bitbucket.org}"
BASEREPOURL="${BASEREPOURL:-git@${REPOHOST}:sunlightio/${REPODIR}.git}"

atexit () {
  if [ $? -ne 0 ]; then
	echo "ERROR: Evolve update FAILED!!!" >&2
	if [[ ${RMWORKPATH} = true && -d ${WORKPATH}/${REPODIR} ]] ; then
		rm -fr ${WORKPATH}/${REPODIR} ; sync
	fi
  fi
}
trap atexit EXIT

updategitbin() {
  if [[ `pwd` == ${VAGRHOME} && ! -e ${WORKPATH} ]]; then
	mkdir -p ${WORKPATH} || exit -2
  elif [[ ! -e ${WORKPATH} ]]; then
	WORKPATH=$(mktemp -d || exit -3)
	RMWORKPATH=true
  fi
  if [ -d ${WORKPATH} ]; then
	cd ${WORKPATH} || exit -4 
	if [ ! -d ${WORKPATH}/${REPODIR} ]; then
		# requires zeppelinvm ssh key and proper .ssh/config
		git clone ${BASEREPOURL} || exit -5 
	fi
	(cd ${REPODIR} && git pull) || exit -6
	(cd ${REPODIR} && make update) || exit -7
  else
	echo "ERROR: could not find workpath, aborting!" && exit -8
  fi
}
updategitbin || (echo "ERROR: aborting!" && exit -1 )
